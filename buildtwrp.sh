#!/bin/bash
echo " +----------------------------------------------------------+"$'\n' \
  $'| NetHunter bootanimation TWRP zip creator 1.0 by yesimxev |'$'\n' \
  $'^==========================================================^'$'\n'
echo "Copying created zip to TWRP files.."
cp output/bootanimation.zip twrp_files/system/media/
cd twrp_files
echo "Creating zip.."
zip -0 -r -q ../output/nh-bootanimation-twrp-installer.zip META-INF system
if [ -d "/sdcard" ]
then
	echo "Copying to /sdcard.." && cp ../output/nh-bootanimation-twrp-installer.zip /sdcard/ && echo "" && echo "Bootanimation TWRP zip has been created and copied to /sdcard!"
else
	echo "" && echo "Bootanimation TWRP zip has been copied to output folder!"
fi
rm -r ../twrp_files/system/media/bootanimation.zip
